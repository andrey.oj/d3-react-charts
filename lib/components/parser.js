"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseBarChartData = parseBarChartData;
exports.parsePieChartData = parsePieChartData;
exports.parserLineData = parserLineData;

var d3 = _interopRequireWildcard(require("d3"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var HOLDER_X = ['name', 'x'];
var HOLDER_Y = ['value', 'y'];

function findXName(data) {
  if (data.length) {
    for (var i = 0; i < HOLDER_X.length; i += 1) {
      if (data[0][HOLDER_X[i]]) return HOLDER_X[i];
    }
  } else {
    for (var _i = 0; _i < HOLDER_X.length; _i += 1) {
      if (data[HOLDER_X[_i]]) return HOLDER_X[_i];
    }
  }

  return HOLDER_X[0];
}

function findYName(data) {
  if (data.length) {
    for (var i = 0; i < HOLDER_Y.length; i += 1) {
      if (data[0][HOLDER_Y[i]]) return HOLDER_Y[i];
    }
  } else {
    for (var _i2 = 0; _i2 < HOLDER_Y.length; _i2 += 1) {
      if (data[HOLDER_Y[_i2]]) return HOLDER_Y[_i2];
    }
  }

  return HOLDER_Y[0];
}

function parsePieChartData(rawData, yName, xName) {
  var data = rawData;
  if (rawData.length === 0) return {
    data: data,
    keys: [],
    total: 0,
    yName: yName,
    xName: xName
  };
  var total = 0;
  yName = yName || findYName(rawData);
  xName = xName || findXName(rawData);
  var keys;

  if (rawData.length) {
    total = rawData.map(function (d) {
      return d[yName];
    }).reduce(function (a, b) {
      return a + b;
    });
    keys = rawData.map(function (d) {
      return d[xName];
    }).filter(function (d, i, s) {
      return s.indexOf(d) === i;
    });
    data = keys.map(function (k) {
      return {
        name: k,
        value: rawData.filter(function (d) {
          return d[xName] === k;
        }).map(function (d) {
          return +d[yName];
        }).reduce(function (a, b) {
          return a + b;
        })
      };
    });
  } else {
    keys = Object.keys(rawData);
    data = keys.map(function (k) {
      return {
        name: k,
        value: rawData[k]
      };
    });
    total = data.map(function (d) {
      return d.value;
    }).reduce(function (a, b) {
      return a + b;
    });
  }

  return {
    data: data,
    keys: keys,
    total: total,
    yName: yName,
    xName: xName
  };
}

function parseBarChartData(rawData, transposed) {
  var n, m, keys, range;
  var data = rawData; //  Is Array

  if (rawData.length) {
    var example = rawData[0];
    n = rawData.length;
    m = example.length; //  Is Array

    if (example.length) {
      /**
       * [[{name:'Windows', value: 120},...]...]
       * */
      if (_typeof(example[0]) === 'object') {
        var xName = findXName(example);
        var yName = findYName(example);

        if (example[0][xName] && example[0][yName]) {
          keys = example.map(function (d) {
            return d[xName];
          });
          data = rawData.map(function (d) {
            return d.map(function (x) {
              return x[yName];
            });
          });
        } //  Else error

      }
      /**
       * [[120,343,343], [343,321,54],..]
       * */
      else {
          keys = d3.range(m);
        }
    } //  Is Object
    else {
        //  Is Real Object

        /**
         * [{Windows: 120, Linux: 234},..]
         * **/
        if (_typeof(rawData[0]) === 'object') {
          keys = Object.keys(rawData[0]);
          m = rawData.length;
          n = keys.length;
          data = keys.map(function (k) {
            return rawData.map(function (d) {
              return d[k];
            });
          });
        } //  Is Number

        /**
         * [120,345,323...]
         * */
        else {
            m = 1;
            keys = [0];
            data = [rawData];
          }
      }

    range = d3.range(m);
  } //  Is Object
  else {
      keys = Object.keys(rawData);
      n = keys.length;
      m = rawData[keys[0]].length;
      data = keys.map(function (k) {
        return rawData[k];
      });
      range = d3.range(m);
    }

  var titles = keys;

  if (transposed) {
    var mTemp = m;
    m = n;
    n = mTemp;
    range = d3.range(m);
    data = d3.transpose(data);
    titles = range;
  }

  return {
    data: data,
    n: n,
    m: m,
    keys: keys,
    range: range,
    titles: titles
  };
}

function parserLineData(rawData, stack) {
  var xName = 'date';
  var xType = 'date';
  if (!rawData.length) return {
    data: [],
    xName: '',
    xType: '',
    yNames: []
  };
  var examaple = rawData[0];

  if (examaple.date) {//date
  } else if (examaple.x !== undefined) {
    xName = 'x';
    xType = 'x';
  } else if (examaple.day !== undefined) {
    xName = 'day';
    xType = 'x';
  } else if (!examaple.date) {
    xType = 'index';
    xName = 'x';
  }

  var yNames = Object.keys(examaple).filter(function (k) {
    return k !== xName;
  });

  var lastValue = _objectSpread({}, examaple);

  var data;

  if (!stack) {
    data = rawData.map(function (d, i) {
      return _objectSpread({}, d, _defineProperty({}, xName, xType === 'index' ? i : d[xName]));
    });
  } else {
    data = rawData.map(function (d, i) {
      var values = {};
      yNames.forEach(function (n) {
        values[n] = d[n] + (i === 0 ? 0 : lastValue[n]);
      });
      lastValue = d;
      return _objectSpread({}, values, _defineProperty({}, xName, xType === 'index' ? i : d[xName]));
    });
  }

  data.sort(function (a, b) {
    return a[xName] - b[xName];
  });
  return {
    data: data,
    yNames: yNames,
    xName: xName,
    xType: xType
  };
}