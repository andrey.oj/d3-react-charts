"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var d3 = _interopRequireWildcard(require("d3"));

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var COLOR_CHANGE_FACTOR = 3;
var PADDING_TEXT = 2;

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

function rgbToHex(_ref) {
  var r = _ref.r,
      g = _ref.g,
      b = _ref.b;
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function brighter(color, i, maxCount) {
  var dx = {
    r: Math.round((255 - color.r) * (1 / (maxCount * COLOR_CHANGE_FACTOR))),
    g: Math.round((255 - color.g) * (1 / (maxCount * COLOR_CHANGE_FACTOR))),
    b: Math.round((255 - color.b) * (1 / (maxCount * COLOR_CHANGE_FACTOR)))
  };
  return {
    r: color.r + i * dx.r,
    g: color.g + i * dx.g,
    b: color.b + i * dx.b
  };
}

function getTextWidth(text, fontSize, fontFace) {
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  context.font = fontSize + 'px ' + fontFace;
  return context.measureText(text).width;
}

var getTreeMap = function getTreeMap(data, width, height) {
  return d3.treemap().size([width, height]).padding(1).round(true)(d3.hierarchy(data).sum(function (d) {
    return d.value;
  }).sort(function (a, b) {
    return b.height - a.height || b.value - a.value;
  }));
};

var Treemap =
/*#__PURE__*/
function (_Component) {
  _inherits(Treemap, _Component);

  function Treemap() {
    var _this;

    _classCallCheck(this, Treemap);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Treemap).call(this));

    _this.onClick = function (category, offsetColorIndex) {
      if (!_this.state.category) {
        _this.setState({
          category: category,
          offsetColorIndex: offsetColorIndex
        });
      } else {
        _this.setState({
          category: false,
          offsetColorIndex: 0
        });
      }
    };

    _this.transformData = function (_ref2, category) {
      var data = _ref2.data,
          colorRange = _ref2.colorRange,
          id = _ref2.id;
      var offsetColorIndex = _this.state.offsetColorIndex;
      var dataArray = data;

      if (!data.length) {
        var keys = Object.keys(data);
        if (!keys) return [];
        var obj = data[keys[0]];

        if (typeof obj === 'number') {
          dataArray = keys.map(function (k) {
            return {
              name: k,
              value: data[k],
              category: 'default'
            };
          });
        } else if (obj.value) {
          dataArray = keys.map(function (k) {
            return {
              name: k,
              value: data[k].value,
              category: data[k].category || 'default'
            };
          });
        } else {
          console.error('Bad data structure');
          return [];
        }
      } //  Now prepare data


      var categories = category ? [category] : dataArray.map(function (d) {
        return d.category;
      }).filter(function (d, i, self) {
        return self.indexOf(d) >= i;
      });
      var children = categories.map(function (cat, i) {
        var hex = colorRange[cat] || colorRange[i + offsetColorIndex] || _constants.defaultColorRange[i + offsetColorIndex];
        var baseRgb = hexToRgb(hex);
        var inData = dataArray.filter(function (d) {
          return d.category === cat;
        });
        return {
          name: cat,
          children: inData.map(function (x, nr) {
            var color = brighter(baseRgb, nr, inData.length);
            return {
              name: x.name,
              value: x.value,
              category: cat,
              color: category ? hex : rgbToHex(color)
            };
          })
        };
      });
      return {
        wrappedData: {
          name: id,
          children: children
        },
        categories: categories
      };
    };

    _this.readProps = function (props) {
      var data = props.data,
          width = props.width,
          height = props.height;
      var category = _this.state.category;
      if (!data) return [];

      var _this$transformData = _this.transformData(props, category),
          wrappedData = _this$transformData.wrappedData,
          categories = _this$transformData.categories;

      var root = getTreeMap(wrappedData, width, height);
      var max = root.value;
      var leaves = root.leaves();
      var rects = leaves.map(function (d) {
        var fontSize = 20;
        var width = d.x1 - d.x0;
        var height = d.y1 - d.y0;
        var textWidth = getTextWidth(d.data.name, fontSize);
        if (height < fontSize + PADDING_TEXT * 2) fontSize = 0;else if (width < textWidth * 0.8) {
          fontSize = fontSize * (width - PADDING_TEXT * 2) / textWidth * 0.8;
        }
        return {
          id: d.data.name,
          value: d.data.value,
          category: d.data.category,
          percent: (d.data.value / max * 100).toFixed(1),
          fill: d.data.color,
          fillOpacity: 0.9,
          fontSize: fontSize,
          width: d.x1 - d.x0,
          height: d.y1 - d.y0,
          transform: "translate(".concat(d.x0, ",").concat(d.y0, ")")
        };
      });
      return {
        rects: rects,
        categories: categories
      };
    };

    _this.state = {
      category: false,
      offsetColorIndex: 0,
      categories: []
    };
    return _this;
  }

  _createClass(Treemap, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          width = _this$props.width,
          height = _this$props.height,
          labels = _this$props.labels;

      var _this$readProps = this.readProps(this.props),
          rects = _this$readProps.rects,
          categories = _this$readProps.categories;

      return _react.default.createElement("svg", {
        width: width,
        height: height
      }, _react.default.createElement("g", {
        transform: "translate(0,0)"
      }, rects.map(function (p, i) {
        return _react.default.createElement("g", {
          transform: p.transform,
          key: "_path_".concat(p.id, "_").concat(i)
        }, _react.default.createElement("rect", {
          onClick: function onClick() {
            return _this2.onClick(p.category, categories.indexOf(p.category));
          },
          width: p.width,
          height: p.height,
          fill: p.fill
        }), _react.default.createElement("title", null, p.id, ": ", p.value, " - ", p.percent, "%"), labels ? _react.default.createElement("text", {
          style: {
            fontSize: "".concat(p.fontSize, "px")
          },
          opacity: 0.8,
          fontWeight: "bold",
          fill: "#f0f0f0",
          dy: "0.35em"
        }, _react.default.createElement("tspan", {
          x: PADDING_TEXT,
          y: p.fontSize / 2 + PADDING_TEXT
        }, p.id)) : null, labels ? _react.default.createElement("text", {
          style: {
            fontSize: "".concat(p.fontSize * 0.7, "px")
          },
          opacity: 0.9,
          fontWeight: "bold",
          fill: "#f0f0f0",
          dy: "1.35em"
        }, _react.default.createElement("tspan", {
          x: PADDING_TEXT,
          y: p.fontSize / 2 + PADDING_TEXT
        }, p.value, " - ", p.percent, "%")) : null);
      })));
    }
  }]);

  return Treemap;
}(_react.Component);

var _default = Treemap;
exports.default = _default;
Treemap.propTypes = {
  data: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  width: _propTypes.default.number.isRequired,
  height: _propTypes.default.number.isRequired,
  labels: _propTypes.default.bool,
  colorRange: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array])
};
Treemap.defaultProps = {
  labels: true,
  colorRange: _constants.defaultColorRange
};