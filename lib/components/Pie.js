"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var d3 = _interopRequireWildcard(require("d3"));

var _constants = require("./constants");

var _parser = require("./parser");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var pie = d3.pie().sort(null).value(function (d) {
  return d.value;
});

var Pie =
/*#__PURE__*/
function (_Component) {
  _inherits(Pie, _Component);

  function Pie() {
    var _this;

    _classCallCheck(this, Pie);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Pie).call(this));

    _this.onClick = function () {
      if (_this.props.onClick) {
        _this.setState({
          highlightedIndex: -1
        });

        _this.props.onClick({
          key: _this.state.highlightedName,
          value: _this.state.highlightedValue
        });
      }
    };

    _this.onMouseOver = function (highlightedIndex, highlightedName, highlightedValue) {
      _this.setState({
        highlightedIndex: highlightedIndex,
        highlightedName: highlightedName,
        highlightedValue: highlightedValue
      });
    };

    _this.onMouseLeave = function () {
      _this.setState({
        highlightedIndex: -1
      });
    };

    _this.readProps = function (props) {
      var _ref = _this.state ? _this.state : {},
          highlightedIndex = _ref.highlightedIndex;

      var rawData = props.data,
          width = props.width,
          height = props.height,
          colorRange = props.colorRange,
          labels = props.labels,
          fontSize = props.fontSize,
          innerRadius = props.innerRadius,
          margin = props.margin,
          totalColor = props.totalColor,
          expandHighlightRadius = props.expandHighlightRadius;
      if (!rawData || !Object.keys(rawData).length) return {
        errorMessage: 'no data'
      };
      if (!width) return {
        errorMessage: 'width not set'
      };
      if (!height) return {
        errorMessage: 'height not set'
      };
      _this.appliedMargin = margin || _constants.defaultMargin;

      var _parsePieChartData = (0, _parser.parsePieChartData)(rawData, props.yName, props.xName),
          total = _parsePieChartData.total,
          data = _parsePieChartData.data;

      var radius = Math.min(width - margin.right - margin.left, height - margin.bottom - margin.top) / 2;
      var fZ = fontSize ? fontSize : radius / 7;
      var arc = d3.arc().outerRadius(radius).innerRadius(radius * innerRadius);
      var arcHighlighted = d3.arc().outerRadius(radius * expandHighlightRadius).innerRadius(radius * innerRadius * expandHighlightRadius);
      var tr = (width > height ? width - height / 2 - margin.right : width / 2 - margin.right) + margin.left;
      var td = (width > height ? height / 2 - margin.bottom : height - width / 2 - margin.bottom) + margin.top;
      var arcs = pie(data);
      arcs.sort(function (a, b) {
        return b.data.value - a.data.value;
      });
      var paths = arcs.map(function (a, i) {
        var path = i === highlightedIndex ? arcHighlighted(a) : arc(a);
        return {
          path: path,
          fill: colorRange[a.data.name] || colorRange[i] || _constants.defaultColorRange[i],
          name: a.data.name,
          value: a.data.value
        };
      });
      var tLen = "".concat(total).length;
      var totals = highlightedIndex >= 0 ? {
        text: "".concat(paths[highlightedIndex].value),
        name: "".concat(paths[highlightedIndex].name),
        percent: "".concat((paths[highlightedIndex].value / total * 100).toFixed(2), " %"),
        fill: paths[highlightedIndex].fill,
        transform: "translate(-".concat(tLen * fZ / 2, ",-").concat(fZ / 2, ")")
      } : {
        text: "".concat(total),
        fill: totalColor,
        transform: "translate(-".concat(tLen * fZ / 2, ",-").concat(fZ / 2, ")")
      };

      if (labels) {
        var texts = arcs.map(function (a, i) {
          return {
            text: "".concat(a.data.name),
            value: a.data.value,
            fill: colorRange[a.data.name] || colorRange[i] || _constants.defaultColorRange[i],
            transform: "translate(".concat(-tr + margin.left, ",").concat(i * fZ * 1.2 - td + fZ + margin.top, ")"),
            opacity: highlightedIndex === i ? 1 : 0.8
          };
        });
        return {
          paths: paths,
          texts: texts,
          tr: tr,
          td: td,
          totals: totals,
          fontSize: fZ,
          errorMessage: null
        };
      }

      return {
        paths: paths,
        texts: [],
        totals: totals,
        fontSize: fZ,
        errorMessage: null,
        tr: tr,
        td: td
      };
    };

    _this.state = {
      highlightedIndex: -1,
      highlightedName: '',
      highlightedValue: 0
    };
    return _this;
  }

  _createClass(Pie, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          width = _this$props.width,
          height = _this$props.height;

      var _this$readProps = this.readProps(this.props),
          errorMessage = _this$readProps.errorMessage,
          paths = _this$readProps.paths,
          texts = _this$readProps.texts,
          totals = _this$readProps.totals,
          fontSize = _this$readProps.fontSize,
          tr = _this$readProps.tr,
          td = _this$readProps.td;

      if (errorMessage) return _react.default.createElement("h3", {
        style: {
          color: 'red'
        }
      }, errorMessage);
      return _react.default.createElement("svg", {
        width: width,
        height: height,
        onClick: this.onClick,
        onMouseLeave: this.onMouseLeave
      }, _react.default.createElement("g", {
        transform: "translate(".concat(tr, ",").concat(td, ")")
      }, paths.map(function (p, i) {
        return _react.default.createElement("path", {
          key: "_path_".concat(i),
          d: p.path,
          fill: p.fill,
          onMouseOver: function onMouseOver() {
            return _this2.onMouseOver(i, p.name, p.value);
          }
        }, _react.default.createElement("title", null, p.name, " ", p.value));
      }), " ", texts.map(function (t, i) {
        return _react.default.createElement("text", {
          onMouseOver: function onMouseOver() {
            return _this2.onMouseOver(i, t.text, t.value);
          },
          style: {
            fontSize: fontSize,
            opacity: t.opacity
          },
          key: "_t1_".concat(i),
          fill: t.fill,
          transform: t.transform,
          dy: "0.35em"
        }, _react.default.createElement("tspan", {
          x: 0
        }, t.text));
      }), totals ? _react.default.createElement("text", {
        style: {
          fontSize: "".concat(fontSize * 2, "px")
        },
        fontWeight: "bold",
        fill: totals.fill,
        textAnchor: "middle",
        dy: "0.35em"
      }, _react.default.createElement("tspan", {
        x: 0,
        y: 0
      }, totals.text)) : null, totals && totals.name ? _react.default.createElement("text", {
        style: {
          fontSize: "".concat(fontSize, "px"),
          opacity: 0.7
        },
        fontWeight: "bold",
        fill: totals.fill,
        textAnchor: "middle",
        dy: "0.35em"
      }, _react.default.createElement("tspan", {
        x: 0,
        y: -fontSize * 2
      }, totals.name)) : null, totals && totals.percent ? _react.default.createElement("text", {
        style: {
          fontSize: "".concat(fontSize, "px"),
          opacity: 0.7
        },
        fontWeight: "bold",
        fill: totals.fill,
        textAnchor: "middle",
        dy: "0.35em"
      }, _react.default.createElement("tspan", {
        x: 0,
        y: fontSize * 2
      }, totals.percent)) : null));
    }
  }]);

  return Pie;
}(_react.Component);

var _default = Pie;
exports.default = _default;
Pie.propTypes = {
  data: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  width: _propTypes.default.number.isRequired,
  height: _propTypes.default.number.isRequired,
  innerRadius: _propTypes.default.number,
  labels: _propTypes.default.bool,
  totalColor: _propTypes.default.string,
  margin: _propTypes.default.object,
  expandHighlightRadius: _propTypes.default.number,
  yName: _propTypes.default.string,
  xName: _propTypes.default.string,
  onClick: _propTypes.default.func
};
Pie.defaultProps = {
  colorRange: _constants.defaultColorRange,
  labels: true,
  innerRadius: 0.7,
  fontSize: null,
  totalColor: '#a0a0a0',
  expandHighlightRadius: 1.1,
  onClick: null,
  margin: {
    right: 20,
    left: 20,
    top: 20,
    bottom: 20
  },
  xName: 'name',
  yName: 'value'
};