"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var d3 = _interopRequireWildcard(require("d3"));

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var LineChart =
/*#__PURE__*/
function (_Component) {
  _inherits(LineChart, _Component);

  function LineChart(props) {
    var _this;

    _classCallCheck(this, LineChart);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LineChart).call(this));

    _initialiseProps.call(_assertThisInitialized(_assertThisInitialized(_this)));

    var width = props.width,
        height = props.height,
        margin = props.margin;
    _this.appliedMargin = margin || _constants.defaultMargin;
    _this.absHeight = height - _this.appliedMargin.top - _this.appliedMargin.bottom;
    _this.absWidth = width - _this.appliedMargin.left - _this.appliedMargin.right;
    _this.xScale = d3.scaleLinear().rangeRound([_this.appliedMargin.left, _this.absWidth]);
    _this.yScale = d3.scaleLinear().rangeRound([_this.absHeight, _this.appliedMargin.top]);
    _this.xAxis = d3.axisBottom(_this.xScale).tickFormat(function (d) {
      return "".concat(d);
    });
    _this.yAxis = d3.axisLeft(_this.yScale).tickFormat(function (d) {
      return "".concat(d);
    });
    _this.formatDate = d3.timeFormat("%d-%b-%y");
    _this.state = {
      pointer: null,
      zoom: {
        isZooming: false,
        from: null,
        to: null
      }
    };
    return _this;
  }

  _createClass(LineChart, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(props) {
      var _this2 = this;

      var xName = props.xName,
          data = props.data,
          type = props.type;

      if (data && data.length) {
        if (type === 'line') {
          this.pathGenerator = d3.line().x(function (d) {
            return _this2.xScale(d[xName]);
          }).y(function (d) {
            return _this2.yScale(d.value);
          });
        } else if (type === 'area') {
          this.pathGenerator = d3.area().x(function (d) {
            return _this2.xScale(d[xName]);
          }).y0(this.yScale(0)).y1(function (d) {
            return _this2.yScale(d.value);
          });
        }

        this.data = data;
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.refs.xAxis) {
        d3.select(this.refs.xAxis).call(this.xAxis);
      }

      if (this.refs.yAxis) {
        d3.select(this.refs.yAxis).call(this.yAxis);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          width = _this$props.width,
          height = _this$props.height,
          id = _this$props.id,
          margin = _this$props.margin,
          type = _this$props.type;

      var _this$readProps = this.readProps(this.props),
          lines = _this$readProps.lines,
          absMin = _this$readProps.absMin,
          absMax = _this$readProps.absMax;

      var zoom = this.state.zoom;
      var minLine = absMin > 0 ? absMin : 0;
      if (absMax < 0) minLine = absMin;
      return _react.default.createElement("svg", {
        key: id,
        width: width,
        height: height,
        onClick: this.onClick,
        onMouseUp: this.handleMouseUp,
        onMouseDown: this.handleMouseDown,
        onMouseMove: this.handleMouseMove
      }, lines.map(function (graph, i) {
        return _react.default.createElement("g", {
          key: "group_".concat(graph.name)
        }, _react.default.createElement("path", {
          key: "p_".concat(graph.name),
          transform: graph.transform,
          d: graph.path,
          fill: type === 'area' ? graph.color : 'none',
          strokeLinejoin: "round",
          strokeLinecap: "round",
          strokeWidth: 1.5,
          stroke: graph.color
        }), _react.default.createElement("text", {
          key: "text_".concat(graph.name),
          style: {
            fontSize: "".concat(12, "px")
          },
          fill: graph.color,
          transform: "translate(".concat(_this3.appliedMargin.left + 10, ",").concat(_this3.appliedMargin.top + 30 + i * 14, ")")
        }, _react.default.createElement("tspan", {
          x: 0,
          y: 0
        }, graph.name)));
      }), zoom.isZooming && zoom.overlay ? _react.default.createElement("g", null, _react.default.createElement("rect", {
        transform: "translate(".concat(zoom.overlay.start, ",").concat(margin.top, ")"),
        width: zoom.overlay.width,
        height: height - margin.bottom - margin.top,
        opacity: 0.3,
        fill: "#909090"
      }), _react.default.createElement("text", {
        style: {
          fontSize: "".concat(12, "px")
        },
        fill: "#404040",
        transform: "translate(".concat(zoom.overlay.start, ",").concat(margin.top, ")")
      }, _react.default.createElement("tspan", {
        x: 2,
        y: 20
      }, zoom.overlay.fromX, " - ", zoom.overlay.toX))) : null, _react.default.createElement("g", null, _react.default.createElement("g", {
        ref: "xAxis",
        transform: "translate(0,".concat(this.yScale(minLine) + this.appliedMargin.top, ")")
      }), _react.default.createElement("g", {
        ref: "yAxis",
        transform: "translate(".concat(this.appliedMargin.left, ",").concat(this.appliedMargin.top, ")")
      })));
    }
  }]);

  return LineChart;
}(_react.Component);

var _initialiseProps = function _initialiseProps() {
  var _this4 = this;

  this.getMousePosition = function (event) {
    var nativeEvent = event.nativeEvent;
    var margin = _this4.props.margin;
    var offsetX = nativeEvent.offsetX,
        offsetY = nativeEvent.offsetY;

    var x = _this4.xScale.invert(offsetX);

    var y = _this4.yScale.invert(offsetY);

    return {
      x: x,
      y: y,
      offsetX: offsetX - margin.left,
      offsetY: offsetY
    };
  };

  this.handleMouseDown = function (event) {
    var _this4$getMousePositi = _this4.getMousePosition(event),
        x = _this4$getMousePositi.x,
        y = _this4$getMousePositi.y,
        offsetX = _this4$getMousePositi.offsetX;

    if (_this4.state.zoom.isZooming || _this4.state.zoom.to) {
      _this4.resetZoom();

      return;
    }

    var zoom = {
      isZooming: true,
      from: {
        x: x,
        y: y,
        offsetX: offsetX
      },
      to: null
    };

    _this4.setState({
      zoom: zoom
    });
  };

  this.handleMouseUp = function (event) {
    var _this4$getMousePositi2 = _this4.getMousePosition(event),
        x = _this4$getMousePositi2.x,
        y = _this4$getMousePositi2.y,
        offsetX = _this4$getMousePositi2.offsetX;

    if (!_this4.state.zoom.from) return;
    var xMin = Math.min(x, _this4.state.zoom.from.x);
    var xMax = Math.max(x, _this4.state.zoom.from.x);
    var yMin = Math.min(y, _this4.state.zoom.from.y);
    var yMax = Math.max(y, _this4.state.zoom.from.y);
    var zoom = {
      isZooming: false,
      from: {
        x: xMin,
        y: yMin,
        offsetX: offsetX
      },
      to: {
        y: yMax,
        x: xMax
      },
      overlay: null
    };

    _this4.setState({
      zoom: zoom
    });
  };

  this.handleMouseMove = function (event) {
    var _this4$getMousePositi3 = _this4.getMousePosition(event),
        x = _this4$getMousePositi3.x,
        offsetX = _this4$getMousePositi3.offsetX;

    var margin = _this4.props.margin;
    var zoom = _this4.state.zoom;
    var isZooming = zoom.isZooming,
        from = zoom.from;

    if (isZooming && from) {
      var width = Math.abs(from.offsetX - offsetX);

      if (offsetX > from.offsetX) {
        zoom.overlay = {
          width: width,
          start: from.offsetX + margin.left,
          fromX: from.x.toFixed(1),
          toX: x.toFixed(1)
        };
      } else {
        zoom.overlay = {
          width: width,
          start: offsetX + margin.left,
          fromX: x.toFixed(1),
          toX: from.x.toFixed(1)
        };
      }
    } else {
      zoom.overlay = null;
    }

    _this4.setState({
      zoom: zoom
    });
  };

  this.resetZoom = function () {
    _this4.setState({
      zoom: {
        isZooming: false,
        from: null,
        to: null,
        overlay: null
      }
    });
  };

  this.onClick = function () {
    var _this4$state$zoom = _this4.state.zoom,
        from = _this4$state$zoom.from,
        to = _this4$state$zoom.to;

    if (!from || !to) {
      _this4.resetZoom();

      return;
    }

    var delta = Math.abs(from.x - to.x) + Math.abs(from.y - to.y);

    if (delta < 4) {
      _this4.resetZoom();
    }
  };

  this.filterData = function (data, zoom, valueKeys, xName) {
    return data.filter(function (d) {
      return d[xName] > zoom.from.x && d[xName] <= zoom.to.x;
    });
  };

  this.readProps = function (props) {
    var lines = [];
    var zoom = _this4.state.zoom;
    var width = props.width,
        height = props.height,
        colorRange = props.colorRange,
        xName = props.xName,
        type = props.type,
        stacked = props.stacked,
        yNames = props.yNames;
    var data = _this4.data;
    if (!data || !data.length || !yNames) return {
      lines: lines
    };
    if (!width) return {
      lines: lines
    };
    if (!height) return {
      lines: lines
    };
    var appliedData = data;

    if (zoom.to && !zoom.isZooming) {
      appliedData = _this4.filterData(data, zoom, yNames, xName);
    }

    if (!appliedData || !appliedData.length) return {
      lines: lines,
      absMin: 0
    };
    var absMax = d3.max(appliedData, function (d) {
      return d3.max(yNames.map(function (k) {
        return d[k];
      }));
    });
    var absMin = d3.min(appliedData, function (d) {
      return d3.min(yNames.map(function (k) {
        return d[k];
      }));
    }); //const stackedData = d3.stack().values(d=>appliedData);
    // Update scales

    _this4.yScale.domain([absMin, absMax]);

    _this4.xScale.domain(d3.extent(appliedData, function (d) {
      return d[xName];
    }));

    yNames.forEach(function (k, i) {
      var relData = appliedData.map(function (d) {
        var _ref;

        return _ref = {}, _defineProperty(_ref, xName, d[xName]), _defineProperty(_ref, "value", d[k]), _ref;
      });
      var color = colorRange[k] || colorRange[i] || _constants.defaultColorRange[i];

      var path = _this4.pathGenerator(relData);

      lines.push({
        name: k,
        path: path,
        color: color,
        data: relData,
        transform: "translate(".concat(0, ",", _this4.appliedMargin.top, ")")
      });
    });
    return {
      lines: lines,
      absMin: absMin,
      absMax: absMax
    };
  };
};

var _default = LineChart;
exports.default = _default;
LineChart.propTypes = {
  data: _propTypes.default.array.isRequired,
  id: _propTypes.default.string.isRequired,
  width: _propTypes.default.number.isRequired,
  height: _propTypes.default.number.isRequired,
  xName: _propTypes.default.string.isRequired,
  margin: _propTypes.default.object.isRequired,
  colorRange: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  fontSize: _propTypes.default.number.isRequired,
  type: _propTypes.default.string.isRequired,
  yNames: _propTypes.default.array.isRequired,
  stacked: _propTypes.default.bool.isRequired
};
LineChart.defaultProps = {};