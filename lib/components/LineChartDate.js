"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var d3 = _interopRequireWildcard(require("d3"));

var _LineChartNumber2 = _interopRequireDefault(require("./LineChartNumber"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var LineChart =
/*#__PURE__*/
function (_LineChartNumber) {
  _inherits(LineChart, _LineChartNumber);

  function LineChart(props) {
    var _this;

    _classCallCheck(this, LineChart);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LineChart).call(this, props));

    _initialiseProps.call(_assertThisInitialized(_assertThisInitialized(_this)));

    var xName = props.xName;
    _this.xScale = d3.scaleTime().rangeRound([_this.appliedMargin.left, _this.absWidth]);
    _this.xAxis = d3.axisBottom(_this.xScale).tickFormat(d3.timeFormat('%d-%b'));
    _this.formatDate = d3.timeFormat("%d-%b-%y"); // Lines

    _this.lineGenerator = d3.line().x(function (d) {
      return _this.xScale(d[xName]);
    }).y(function (d) {
      return _this.yScale(d.value);
    });
    return _this;
  }

  _createClass(LineChart, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.refs.xAxis) {
        d3.select(this.refs.xAxis).call(this.xAxis).selectAll("text").attr("dy", "1.7em").attr("dx", "1.7em").attr("transform", "rotate(45)");
      }

      if (this.refs.yAxis) {
        d3.select(this.refs.yAxis).call(this.yAxis);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          width = _this$props.width,
          height = _this$props.height,
          id = _this$props.id,
          margin = _this$props.margin,
          fontSize = _this$props.fontSize;

      var _this$readProps = this.readProps(this.props),
          lines = _this$readProps.lines;

      var zoom = this.state.zoom;
      return _react.default.createElement("svg", {
        key: id,
        width: width,
        height: height,
        onClick: this.onClick,
        onMouseUp: this.handleMouseUp,
        onMouseDown: this.handleMouseDown,
        onMouseMove: this.handleMouseMoveX
      }, lines.map(function (graph, i) {
        return _react.default.createElement("g", {
          key: "group_".concat(graph.name)
        }, _react.default.createElement("path", {
          key: "p_".concat(graph.name),
          transform: graph.transform,
          d: graph.path,
          fill: "none",
          strokeLinejoin: "round",
          strokeLinecap: "round",
          strokeWidth: 1.5,
          stroke: graph.color
        }), _react.default.createElement("text", {
          key: "text_".concat(graph.name),
          style: {
            fontSize: "".concat(12, "px")
          },
          fill: graph.color,
          transform: "translate(".concat(_this2.appliedMargin.left + 10, ",").concat(_this2.appliedMargin.top + 30 + i * 14, ")")
        }, _react.default.createElement("tspan", {
          x: 0,
          y: 0
        }, graph.name)));
      }), zoom.isZooming && zoom.overlay ? _react.default.createElement("g", null, _react.default.createElement("rect", {
        transform: "translate(".concat(zoom.overlay.start, ",").concat(margin.top, ")"),
        width: zoom.overlay.width,
        height: height - margin.bottom - margin.top,
        opacity: 0.3,
        fill: "#909090"
      }), _react.default.createElement("text", {
        style: {
          fontSize: "".concat(fontSize, "px")
        },
        fill: "#404040",
        transform: "translate(".concat(zoom.overlay.start, ",").concat(margin.top, ")")
      }, _react.default.createElement("tspan", {
        x: 2,
        y: fontSize * 2
      }, "From ", zoom.overlay.fromX)), _react.default.createElement("text", {
        style: {
          fontSize: "".concat(fontSize, "px")
        },
        fill: "#404040",
        transform: "translate(".concat(zoom.overlay.start, ",").concat(margin.top, ")")
      }, _react.default.createElement("tspan", {
        x: 2,
        y: fontSize * 4
      }, "To ", zoom.overlay.toX))) : null, _react.default.createElement("g", null, _react.default.createElement("g", {
        ref: "xAxis",
        transform: "translate(0,".concat(this.yScale(0) + this.appliedMargin.top, ")")
      }), _react.default.createElement("g", {
        ref: "yAxis",
        transform: "translate(".concat(this.appliedMargin.left, ",").concat(margin.top, ")")
      })));
    }
  }]);

  return LineChart;
}(_LineChartNumber2.default);

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.handleMouseMoveX = function (event) {
    var _this3$getMousePositi = _this3.getMousePosition(event),
        x = _this3$getMousePositi.x,
        offsetX = _this3$getMousePositi.offsetX;

    var margin = _this3.props.margin;
    var zoom = _this3.state.zoom;
    var isZooming = zoom.isZooming,
        from = zoom.from;

    if (isZooming && from) {
      var width = Math.abs(from.offsetX - offsetX);

      if (offsetX > from.offsetX) {
        zoom.overlay = {
          width: width,
          start: from.offsetX + margin.left,
          fromX: _this3.formatDate(from.x),
          toX: _this3.formatDate(x)
        };
      } else {
        zoom.overlay = {
          width: width,
          start: offsetX + margin.left,
          fromX: _this3.formatDate(x),
          toX: _this3.formatDate(from.x)
        };
      }
    } else {
      zoom.overlay = null;
    }

    _this3.setState({
      zoom: zoom
    });
  };

  this.readProps = function (props) {
    var lines = [];
    var zoom = _this3.state.zoom;
    var data = props.data,
        width = props.width,
        height = props.height,
        colorRange = props.colorRange,
        xName = props.xName;
    if (!data || !data.length) return {
      lines: lines
    };
    var valueKeys = Object.keys(data[0]).filter(function (k) {
      return k !== xName;
    });
    if (!width) return {
      lines: lines
    };
    if (!height) return {
      lines: lines
    };
    var appliedData = data;

    if (zoom.to && !zoom.isZooming) {
      appliedData = _this3.filterData(data, zoom, valueKeys, xName);
    }

    if (!appliedData || !appliedData.length) return {
      lines: lines
    };
    var absMax = d3.max(appliedData, function (d) {
      return d3.max(valueKeys.map(function (k) {
        return d[k];
      }));
    });
    var absMin = d3.min(appliedData, function (d) {
      return d3.min(valueKeys.map(function (k) {
        return d[k];
      }));
    }); // Update scales

    _this3.yScale.domain([absMin, absMax]);

    _this3.xScale.domain(d3.extent(appliedData, function (d) {
      return d[xName];
    }));

    valueKeys.forEach(function (k, i) {
      var relData = appliedData.map(function (d) {
        var _ref;

        return _ref = {}, _defineProperty(_ref, xName, d[xName]), _defineProperty(_ref, "value", d[k]), _ref;
      });
      relData.sort(function (a, b) {
        return a[xName] - b[xName];
      });

      var path = _this3.lineGenerator(relData);

      lines.push({
        name: k,
        path: path,
        color: colorRange[k] || colorRange[i],
        data: relData,
        transform: "translate(".concat(0, ",", _this3.appliedMargin.top, ")")
      });
    });
    return {
      lines: lines
    };
  };
};

var _default = LineChart;
exports.default = _default;
LineChart.propTypes = {
  data: _propTypes.default.array.isRequired,
  id: _propTypes.default.string.isRequired,
  width: _propTypes.default.number.isRequired,
  height: _propTypes.default.number.isRequired,
  xName: _propTypes.default.string.isRequired,
  margin: _propTypes.default.object.isRequired,
  colorRange: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  fontSize: _propTypes.default.number.isRequired,
  xType: _propTypes.default.string.isRequired
};
LineChart.defaultProps = {};