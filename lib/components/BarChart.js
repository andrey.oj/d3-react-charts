"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var d3 = _interopRequireWildcard(require("d3"));

var _constants = require("./constants");

var _parser = require("./parser");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var BarChart =
/*#__PURE__*/
function (_Component) {
  _inherits(BarChart, _Component);

  function BarChart(props) {
    var _this;

    _classCallCheck(this, BarChart);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BarChart).call(this, props));

    _this.handleChangeLayout = function () {
      if (_this.state.layout === 'grouped') {
        _this.setState({
          layout: 'stacked'
        });
      } else {
        _this.setState({
          layout: 'grouped'
        });
      }
    };

    _this.prepareData = function (_ref) {
      var rawData = _ref.data,
          margin = _ref.margin,
          width = _ref.width,
          height = _ref.height;

      var _parseBarChartData = (0, _parser.parseBarChartData)(rawData),
          data = _parseBarChartData.data,
          n = _parseBarChartData.n,
          m = _parseBarChartData.m,
          keys = _parseBarChartData.keys,
          range = _parseBarChartData.range;

      var y01z = d3.stack().keys(d3.range(n))(d3.transpose(data)) // stacked yz
      .map(function (d, i) {
        return d.map(function (_ref2) {
          var _ref3 = _slicedToArray(_ref2, 2),
              y0 = _ref3[0],
              y1 = _ref3[1];

          return [y0, y1, i];
        });
      });
      var yMax = d3.max(data, function (y) {
        return d3.max(y);
      });
      var y1Max = d3.max(y01z, function (y) {
        return d3.max(y, function (d) {
          return d[1];
        });
      });
      var x = d3.scaleBand().domain(range).rangeRound([margin.left, width - margin.right]).padding(0.08);
      var y = d3.scaleLinear().domain([0, y1Max]).range([height - margin.bottom, margin.top]);
      _this.xAxis = d3.axisBottom(x).tickFormat(function (d) {
        return "".concat(d);
      });
      _this.yAxis = d3.axisLeft(y).tickFormat(function (d) {
        return "".concat(d);
      });
      var z = d3.scaleSequential(d3.interpolateBlues).domain([-0.5 * n, 1.5 * n]);
      return {
        x: x,
        y: y,
        z: z,
        y01z: y01z,
        n: n,
        m: m,
        yMax: yMax,
        y1Max: y1Max,
        range: range,
        keys: keys
      };
    };

    _this.renderAxis = function (y) {
      var margin = _this.props.margin;
      return _react.default.createElement("g", null, _react.default.createElement("g", {
        ref: "xAxis",
        transform: "translate(0,".concat(y(0), ")")
      }), _react.default.createElement("g", {
        ref: "yAxis",
        transform: "translate(".concat(margin.left, ",0)")
      }));
    };

    _this.state = {
      layout: props.layout
    };
    return _this;
  }

  _createClass(BarChart, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.refs.xAxis) {
        d3.select(this.refs.xAxis).call(this.xAxis);
      }

      if (this.refs.yAxis) {
        d3.select(this.refs.yAxis).call(this.yAxis);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          width = _this$props.width,
          height = _this$props.height,
          colorRange = _this$props.colorRange;

      var _this$prepareData = this.prepareData(this.props),
          y01z = _this$prepareData.y01z,
          x = _this$prepareData.x,
          y = _this$prepareData.y,
          n = _this$prepareData.n,
          yMax = _this$prepareData.yMax,
          y1Max = _this$prepareData.y1Max,
          keys = _this$prepareData.keys;

      if (!y01z || !n) return null;

      if (this.state.layout === 'grouped') {
        y.domain([0, yMax]);
        return _react.default.createElement("svg", {
          width: width,
          height: height,
          onClick: this.handleChangeLayout
        }, _react.default.createElement("g", null, y01z.map(function (d, i) {
          return _react.default.createElement("g", {
            key: "g_".concat(i),
            fill: colorRange[i]
          }, d.map(function (rec, j) {
            return _react.default.createElement("rect", {
              key: "rec_".concat(j),
              x: x(j) + x.bandwidth() / n * rec[2],
              y: y(rec[1] - rec[0]),
              width: x.bandwidth() / n,
              height: y(0) - y(rec[1] - rec[0])
            }, _react.default.createElement("title", null, keys[i], "  ", rec[1] - rec[0]));
          }));
        })), this.renderAxis(y));
      } else {
        y.domain([0, y1Max]);
        return _react.default.createElement("svg", {
          width: width,
          height: height,
          onClick: this.handleChangeLayout
        }, _react.default.createElement("g", null, y01z.map(function (d, i) {
          return _react.default.createElement("g", {
            key: "g_".concat(i),
            fill: colorRange[i]
          }, d.map(function (rec, j) {
            return _react.default.createElement("rect", {
              key: "rec_".concat(j),
              x: x(j),
              y: y(rec[1]),
              width: x.bandwidth(),
              height: y(rec[0]) - y(rec[1])
            }, _react.default.createElement("title", null, keys[i], "  ", rec[1] - rec[0]));
          }));
        })), this.renderAxis(y));
      }
    }
  }]);

  return BarChart;
}(_react.Component);

var _default = BarChart;
exports.default = _default;
BarChart.propTypes = {
  data: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  width: _propTypes.default.number.isRequired,
  height: _propTypes.default.number.isRequired,
  margin: _propTypes.default.object,
  layout: _propTypes.default.string
};
BarChart.defaultProps = {
  colorRange: _constants.defaultColorRange,
  margin: _constants.defaultMargin,
  layout: 'stacked'
};