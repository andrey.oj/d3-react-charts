"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _LineChartDate = _interopRequireDefault(require("./LineChartDate"));

var _LineChartNumber = _interopRequireDefault(require("./LineChartNumber"));

var _constants = require("./constants");

var _parser = require("./parser");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var LineChart = function LineChart(props) {
  var _parserLineData = (0, _parser.parserLineData)(props.data, props.stacked),
      data = _parserLineData.data,
      xName = _parserLineData.xName,
      xType = _parserLineData.xType,
      yNames = _parserLineData.yNames;

  return xType === 'date' ? _react.default.createElement(_LineChartDate.default, _extends({}, props, {
    xName: xName,
    data: data
  })) : _react.default.createElement(_LineChartNumber.default, _extends({}, props, {
    xName: xName,
    data: data,
    xType: xType,
    yNames: yNames
  }));
};

var _default = LineChart;
exports.default = _default;
LineChart.propTypes = {
  data: _propTypes.default.array.isRequired,
  id: _propTypes.default.string.isRequired,
  width: _propTypes.default.number.isRequired,
  height: _propTypes.default.number.isRequired,
  margin: _propTypes.default.object,
  fontSize: _propTypes.default.number,
  colorRange: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  stacked: _propTypes.default.bool
};
LineChart.defaultProps = {
  fontSize: 12,
  colorRange: _constants.defaultColorRange,
  margin: _constants.defaultMargin,
  type: 'line',
  stacked: false
};