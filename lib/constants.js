"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultMargin = exports.defaultColorRange = void 0;
var defaultColorRange = ['#FF6633', '#6680B3', '#809900', '#00B3E6', '#E6B333', '#3366E6', '#99FF99', '#B34D4D', '#80B300', '#E6B3B3', '#66991A', '#FF99E6', '#CCFF1A', '#FFB399', '#FF1A66', '#E6331A', '#FF33FF', '#FFFF99'];
exports.defaultColorRange = defaultColorRange;
var defaultMargin = {
  right: 20,
  left: 40,
  top: 20,
  bottom: 20
};
exports.defaultMargin = defaultMargin;