import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import {defaultColorRange, defaultMargin} from "./constants";
import {parseBarChartData} from "./parser";


class BarChart extends Component {


  constructor(props) {
    super(props);
    this.state = {
      layout: props.layout
    }

  }

  componentDidUpdate() {
    if(this.refs.xAxis){
      d3.select(this.refs.xAxis).call(this.xAxis);
    }
    if(this.refs.yAxis) {
      d3.select(this.refs.yAxis).call(this.yAxis);
    }
  }

  handleChangeLayout = () => {
    if(this.state.layout === 'grouped'){
      this.setState({layout: 'stacked'});
    }
    else {
      this.setState({layout: 'grouped'});
    }
  }

  prepareData = ({ data: rawData, margin, width, height }) => {

    const {data,n,m, keys, range} = parseBarChartData(rawData);

    const y01z = d3.stack()
      .keys(d3.range(n))
      (d3.transpose(data)) // stacked yz
      .map((d, i) => d.map(([y0, y1]) => [y0, y1, i]));

    const yMax = d3.max(data, y => d3.max(y))
    const y1Max = d3.max(y01z, y => d3.max(y, d => d[1]))

    const x = d3.scaleBand()
      .domain(range)
      .rangeRound([margin.left, width - margin.right])
      .padding(0.08)

    const y = d3.scaleLinear()
      .domain([0, y1Max])
      .range([height - margin.bottom, margin.top])

    this.xAxis = d3.axisBottom(x)
      .tickFormat( d=>`${d}`);
    this.yAxis = d3.axisLeft(y)
      .tickFormat(d=>`${d}`);


    const z = d3.scaleSequential(d3.interpolateBlues)
      .domain([-0.5 * n, 1.5 * n]);

    return {x,y,z,y01z,n,m, yMax, y1Max, range, keys};
  }

  renderAxis = (y) => {
    const { margin } = this.props;

    return (
      <g>
        <g ref="xAxis" transform={`translate(0,${y(0)})`}/>
        <g ref="yAxis" transform={`translate(${margin.left},0)`} />
      </g>
    );
  }

  render() {
    const { width, height,colorRange } = this.props;

    const { y01z, x, y,n, yMax, y1Max, keys } = this.prepareData(this.props);
    if(!y01z || !n) return null;


    if( this.state.layout === 'grouped'){
      y.domain([0, yMax]);

      return (
        <svg width={width} height={height} onClick={this.handleChangeLayout}>
          <g >
            {y01z.map((d, i) =>
              <g key={`g_${i}`} fill={colorRange[i]} >
                {d.map((rec, j) => (
                    <rect
                      key={`rec_${j}`}
                      x={x(j) + x.bandwidth() / n * rec[2]}
                      y={y(rec[1] - rec[0])}
                      width={x.bandwidth()/n}
                      height={y(0) - y(rec[1] - rec[0])}
                    >
                      <title>{keys[i]}  {rec[1] - rec[0]}</title>
                    </rect>
                ))}
              </g >)}
          </g >
          {this.renderAxis(y)}
        </svg >
      );
    }else {
      y.domain([0, y1Max]);

      return (
        <svg width={width} height={height} onClick={this.handleChangeLayout}>
          <g >
            {y01z.map((d, i) =>
              <g key={`g_${i}`} fill={colorRange[i]} >
                {d.map((rec, j) => (
                    <rect
                      key={`rec_${j}`}
                      x={x(j)}
                      y={y(rec[1])}
                      width={x.bandwidth()}
                      height={y(rec[0]) - y(rec[1])}
                    >
                      <title>{keys[i]}  {rec[1] - rec[0]}</title>
                    </rect>
                ))}
              </g >)}
          </g >
          {this.renderAxis(y)}

        </svg >
      );
    }
  }
}

export default BarChart;

BarChart.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  margin: PropTypes.object,
  layout: PropTypes.string,

};

BarChart.defaultProps = {
  colorRange: defaultColorRange,
  margin: defaultMargin,
  layout: 'stacked'

};