const  defaultColorRange =  [
  '#A8443A',
  '#6680B3',
  '#FFC700',
  '#BADAF0',
  '#CFC2ED',
  '#646864',
  '#5D4D29',
  '#A9956E',
  '#78330A',
  '#A2642E',
  '#011627',
  '#2EC4B6',
  '#E71D36',
  '#FF9F1C',
  '#05668D',
  '#028090',
  '#00A896',
  '#02C39A',
];

const defaultMargin = {
  right: 20, left: 40, top:20,bottom:20
};


export {defaultColorRange, defaultMargin};