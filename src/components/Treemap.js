import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import {defaultColorRange} from "./constants";

const COLOR_CHANGE_FACTOR = 3;
const PADDING_TEXT = 2;


function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}
function rgbToHex({r, g, b}) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function brighter(color, i, maxCount) {
  const dx = {
    r: Math.round(((255 -color.r) * (1/(maxCount *COLOR_CHANGE_FACTOR)))),
    g: Math.round(((255 -color.g) *(1/(maxCount *COLOR_CHANGE_FACTOR)))),
    b: Math.round(((255 -color.b) *(1/(maxCount *COLOR_CHANGE_FACTOR)))),

  }

  return {
    r: color.r+i*dx.r,
    g: color.g+i*dx.g,
    b: color.b+i*dx.b,
  }
}

function getTextWidth(text, fontSize, fontFace) {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  context.font = fontSize + 'px ' + fontFace;
  return context.measureText(text).width;
}

const getTreeMap = (data, width, height) => d3.treemap()
  .size([width, height])
  .padding(1)
  .round(true)
  (d3.hierarchy(data)
    .sum(d => d.value)
    .sort((a, b) => b.height - a.height || b.value - a.value));


class Treemap extends Component {

  constructor() {
    super();
    this.state = {
      category: false,
      offsetColorIndex: 0,
      categories: []
    };
  }

  onClick = (category, offsetColorIndex) => {
    if(!this.state.category){
      this.setState({category, offsetColorIndex});
    }else {
      this.setState({category: false, offsetColorIndex: 0});
    }
  }

  transformData = ({data, colorRange, id}, category) => {

    const {offsetColorIndex} = this.state;
    let dataArray = data;

    if(!data.length){
      const keys = Object.keys(data);
      if(!keys) return [];
      const obj = data[keys[0]];
      if(typeof obj === 'number'){
        dataArray = keys.map(k=> ({name: k, value: data[k], category: 'default' }));
      }else if(obj.value){
        dataArray = keys.map(k=> ({name: k, value: data[k].value, category: data[k].category || 'default' }));
      }
      else {
        console.error('Bad data structure');
        return [];
      }

    }

    //  Now prepare data
    const categories =
      category ?
        [category]:
        dataArray.map(d=>d.category).filter((d,i, self) => self.indexOf(d) >= i);

    const children = categories.map((cat,i)=> {
      const hex = colorRange[cat] || colorRange[i+offsetColorIndex] || defaultColorRange[i+offsetColorIndex];
      const baseRgb = hexToRgb(hex);

      const inData = dataArray.filter(d=>d.category === cat);
      return {
        name: cat,
        children: inData.map((x,nr)=>{
          const color = brighter(baseRgb, nr, inData.length);
          return {
            name: x.name,
            value: x.value,
            category: cat,
            color: category ? hex :  rgbToHex(color),
          };
        })
      }
    });

    return {wrappedData: {
      name: id,
      children
    },
      categories}
  }
  readProps =(props) => {

    const {data,width, height} = props;
    const {category} = this.state;

    if(!data) return [];

    const {wrappedData, categories} = this.transformData(props, category);

    const root = getTreeMap(wrappedData,width,height);
    const max = root.value;

    const leaves = root.leaves();

    const rects = leaves.map((d)=> {
      let fontSize = 20;
      const width = d.x1 - d.x0;
      const height = d.y1 - d.y0;

      const textWidth = getTextWidth(d.data.name,fontSize);

      if(height < (fontSize + PADDING_TEXT*2)) fontSize = 0;
      else if(width < textWidth*0.8){
        fontSize = fontSize * (width -PADDING_TEXT*2) / textWidth *0.8;
      }

      return {
        id: d.data.name,
        value: d.data.value,
        category: d.data.category,
        percent: (d.data.value / max *100).toFixed(1),
        fill: d.data.color,
        fillOpacity: 0.9,
        fontSize,
        width: d.x1 - d.x0,
        height: d.y1 - d.y0,
        transform: `translate(${d.x0},${d.y0})`
      };
    });

    return {rects, categories};

  }



  render() {
    const {width, height, labels} = this.props;
    const {rects, categories} = this.readProps(this.props);

    return (
      <svg width={width} height={height}>
        <g transform={`translate(0,0)`}>
          {rects.map((p,i)=>(
            <g transform={p.transform} key={`_path_${p.id}_${i}`}>
              <rect
                onClick={()=>this.onClick(p.category, categories.indexOf(p.category))}
                width={p.width}
                height={p.height}
                fill={p.fill} />

              <title>{p.id}: {p.value} - {p.percent}%</title>

              {labels?
                <text
                  style={{fontSize: `${p.fontSize}px`}}
                  opacity={0.8}
                  fontWeight="bold"
                  fill="#f0f0f0"
                  dy="0.35em">
                  <tspan x={PADDING_TEXT} y={p.fontSize / 2 + PADDING_TEXT}>{p.id}</tspan>
                </text>
                :null}

              {labels?
                <text
                  style={{fontSize: `${p.fontSize*0.7}px`}}
                  opacity={0.9}
                  fontWeight="bold"
                  fill="#f0f0f0"
                  dy="1.35em">
                  <tspan x={PADDING_TEXT} y={p.fontSize / 2 + PADDING_TEXT}>{p.value} - {p.percent}%</tspan>
                </text>
                :null
              }
            </g>
          ))}

        </g>
      </svg>
    );
  }
}

export default Treemap;

Treemap.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  labels : PropTypes.bool,
  colorRange: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

};

Treemap.defaultProps = {
  labels: true,
  colorRange: defaultColorRange,

};