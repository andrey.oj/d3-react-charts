import * as d3 from 'd3';

const HOLDER_X = ['name', 'x'];
const HOLDER_Y = ['value', 'y'];

function findXName(data) {
  if(data.length){
    for( let i=0; i< HOLDER_X.length; i+=1){
      if(data[0][HOLDER_X[i]]) return HOLDER_X[i];
    }
  }else {
    for( let i=0; i< HOLDER_X.length; i+=1){
      if(data[HOLDER_X[i]]) return HOLDER_X[i];
    }
  }
  return HOLDER_X[0];
}

function findYName(data) {
  if(data.length){
    for( let i=0; i< HOLDER_Y.length; i+=1){
      if(data[0][HOLDER_Y[i]]) return HOLDER_Y[i];
    }
  }else {
    for( let i=0; i< HOLDER_Y.length; i+=1){
      if(data[HOLDER_Y[i]]) return HOLDER_Y[i];
    }
  }
  return HOLDER_Y[0];
}

function parsePieChartData(rawData, yName, xName) {

  let data = rawData;
  if(rawData.length === 0) return {data,keys: [], total: 0,yName, xName};
  let total = 0;
  yName = yName || findYName(rawData);
  xName = xName || findXName(rawData);
  let keys;


  if( rawData.length){
    total = rawData.map(d => d[yName]).reduce((a, b) => a + b);
    keys = rawData.map(d=>d[xName]).filter((d,i,s)=> s.indexOf(d) === i);
    data = keys.map(k=> ({
      name: k,
      value: rawData.filter(d=>d[xName] === k).map(d=>+d[yName]).reduce((a,b)=>(a+b))
    }));
  }
  else {
    keys = Object.keys(rawData);
    data = keys.map(k=> ({name: k, value: rawData[k]}));
    total = data.map(d => d.value).reduce((a, b) => a + b);
  }

  return {data,keys, total,yName, xName};
}

function parseBarChartData(rawData,transposed) {
  let n, m,  keys, range;
  let data = rawData;

  //  Is Array
  if(rawData.length){
    const example = rawData[0];
    n= rawData.length;
    m = example.length;
    //  Is Array
    if(example.length ){
      /**
       * [[{name:'Windows', value: 120},...]...]
       * */

      if(typeof (example[0]) === 'object'){
        const xName = findXName(example);
        const yName = findYName(example);

        if(example[0][xName] && example[0][yName]){
          keys = example.map(d=>d[xName]);
          data = rawData.map(d=>d.map(x=>x[yName]));
        }
        //  Else error
      }
      /**
       * [[120,343,343], [343,321,54],..]
       * */
      else {
        keys= d3.range(m);
      }

    }
    //  Is Object
    else {
      //  Is Real Object
      /**
       * [{Windows: 120, Linux: 234},..]
       * **/
      if(typeof (rawData[0]) === 'object'){
        keys = Object.keys(rawData[0]);
        m = rawData.length;
        n= keys.length;
        data = keys.map(k=> rawData.map(d=>d[k]));
      }
      //  Is Number
      /**
       * [120,345,323...]
       * */
      else {
        m= 1;
        keys = [0];
        data = [rawData];
      }

    }
    range = d3.range(m);
  }

  //  Is Object
  else {
    keys = Object.keys(rawData);
    n = keys.length;
    m = rawData[keys[0]].length;
    data = keys.map(k=> rawData[k]);
    range = d3.range(m);
  }

  let titles = keys;

  if(transposed){
    const mTemp = m;
    m = n;
    n = mTemp;
    range = d3.range(m);
    data = d3.transpose(data);
    titles = range;
  }

  return {data,n,m, keys, range, titles};
}

function parserLineData(rawData, stack) {
  let xName = 'date';
  let xType = 'date';

  if(!rawData.length) return {data: [], xName:'', xType:'', yNames: []};

  const examaple = rawData[0];

  if(examaple.date) {
    //date

  }
  else if(examaple.x !== undefined) {
    xName = 'x';
    xType = 'x';
  }
  else if(examaple.day !== undefined) {
    xName = 'day';
    xType = 'x';
  }
  else if(!examaple.date) {
    xType = 'index';
    xName = 'x';
  }

  const yNames = Object.keys(examaple).filter(k=> k!==xName);

  let lastValue = {...examaple};

  let data ;

  if(!stack){
    data = rawData.map((d,i)=> {
      return {
        ...d,
        [xName]: xType === 'index' ? i :  d[xName]
      }
    });

  }
  else {
    data = rawData.map((d,i)=> {

      const values = {};

      yNames.forEach(n=> {values[n] = d[n]+(i === 0 ? 0 : lastValue[n])});

      lastValue = d;
      return {
        ...values,
        [xName]: xType === 'index' ? i :  d[xName]
      }
    });

  }

  data.sort((a,b)=> a[xName]-b[xName]);

  return {data, yNames,xName, xType};
}

export {parseBarChartData, parsePieChartData, parserLineData};