import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import {defaultColorRange, defaultMargin} from "./constants";
import {parsePieChartData} from "./parser";

const pie = d3.pie()
  .sort(null)
  .value(function (d) {
    return d.value;
  });


class Pie extends Component {

  constructor() {
    super();
    this.state = { highlightedIndex: -1, highlightedName: '', highlightedValue: 0};
  }

  onClick = () => {
    if(this.props.onClick){
      this.setState({ highlightedIndex: -1 });
      this.props.onClick({
        key: this.state.highlightedName,
        value: this.state.highlightedValue,
      });
    }
  }
  onMouseOver = (highlightedIndex, highlightedName, highlightedValue) => {
    this.setState({ highlightedIndex,highlightedName,highlightedValue })
  }

  onMouseLeave = () => {
    this.setState({ highlightedIndex: -1 });
  }

  readProps = (props) => {

    const { highlightedIndex } = this.state ? this.state : {};

    const { data: rawData, width, height, colorRange, labels, fontSize, innerRadius, margin, totalColor,expandHighlightRadius } = props;
    if (!rawData || !Object.keys(rawData).length) return { errorMessage: 'no data' };
    if (!width) return { errorMessage: 'width not set' };
    if (!height) return { errorMessage: 'height not set' };
    this.appliedMargin = margin || defaultMargin;

    const {total,data} = parsePieChartData(rawData,props.yName, props.xName);

    const radius = Math.min(width - margin.right - margin.left, height - margin.bottom - margin.top) / 2;
    const fZ = fontSize ? fontSize : radius / 7;

    const arc = d3.arc()
      .outerRadius(radius)
      .innerRadius(radius * innerRadius);
    const arcHighlighted = d3.arc()
      .outerRadius(radius * expandHighlightRadius)
      .innerRadius(radius * innerRadius * expandHighlightRadius);

    const tr = (width > height ? width - height / 2 - margin.right : width / 2 - margin.right) + margin.left;
    const td = (width > height ? height / 2 - margin.bottom : height - width / 2 - margin.bottom) + margin.top;


    const arcs = pie(data);
    arcs.sort((a, b) => b.data.value - a.data.value);

    const paths = arcs.map((a, i) => {

      const path = i === highlightedIndex ? arcHighlighted(a) : arc(a);
      return (
        {
          path,
          fill: colorRange[a.data.name] || colorRange[i] || defaultColorRange[i],
          name: a.data.name,
          value: a.data.value,
        }
      );
    });

    const tLen = `${total}`.length;
    const totals =
      highlightedIndex >= 0 ?
        {
          text: `${paths[highlightedIndex].value}`,
          name: `${paths[highlightedIndex].name}`,
          percent: `${(paths[highlightedIndex].value/total*100).toFixed(2)} %`,
          fill: paths[highlightedIndex].fill,
          transform: `translate(-${tLen * fZ / 2},-${fZ / 2})`

        } :
        {
          text: `${total}`,
          fill: totalColor,
          transform: `translate(-${tLen * fZ / 2},-${fZ / 2})`
        };

    if (labels) {
      const texts = arcs
        .map((a, i) => ({
          text: `${(a.data.name)}`,
          value: a.data.value,
          fill: colorRange[a.data.name] || colorRange[i] || defaultColorRange[i],
          transform: `translate(${-tr + margin.left},${i * fZ * 1.2 - td + fZ + margin.top})`,
          opacity: highlightedIndex === i ? 1 : 0.8,
        }));

      return { paths, texts, tr, td, totals, fontSize: fZ, errorMessage: null };
    }

    return { paths, texts: [], totals, fontSize: fZ, errorMessage: null, tr, td };
  }


  render() {
    const { width, height } = this.props;
    const { errorMessage, paths, texts, totals, fontSize, tr, td } = this.readProps(this.props);
    if (errorMessage) return <h3 style={{ color: 'red' }} >{errorMessage}</h3 >;

    return (
      <svg width={width} height={height}
           onClick={this.onClick}
           onMouseLeave={this.onMouseLeave} >
        <g transform={`translate(${tr},${td})`} >
          {paths.map((p, i) => (
            <path key={`_path_${i}`} d={p.path} fill={p.fill}
                  onMouseOver={() => this.onMouseOver(i,p.name, p.value)} >
              <title >{p.name} {p.value}</title >
            </path >
          ))} {texts.map((t, i) => (
          <text
            onMouseOver={() => this.onMouseOver(i, t.text, t.value)}
            style={{ fontSize, opacity: t.opacity }}
            key={`_t1_${i}`} fill={t.fill} transform={t.transform} dy="0.35em" >
            <tspan x={0} >{t.text}</tspan >
          </text >
        ))}

          {totals ?
            (
              <text
                style={{ fontSize: `${fontSize * 2}px` }}
                fontWeight="bold"
                fill={totals.fill}
                textAnchor="middle"
                dy="0.35em" >
                <tspan x={0} y={0} >{totals.text}</tspan >
              </text >
            )
            : null}
          {
            totals && totals.name ?
              (
                <text
                  style={{ fontSize: `${fontSize}px`, opacity:0.7 }}
                  fontWeight="bold"
                  fill={totals.fill}
                  textAnchor="middle"
                  dy="0.35em" >
                  <tspan x={0} y={-fontSize*2} >{totals.name}</tspan >
                </text >
              )
              : null
          }

          {
            totals && totals.percent ?
              (
                <text
                  style={{ fontSize: `${fontSize}px`, opacity:0.7 }}
                  fontWeight="bold"
                  fill={totals.fill}
                  textAnchor="middle"
                  dy="0.35em" >
                  <tspan x={0} y={fontSize*2} >{totals.percent}</tspan >
                </text >
              )
              : null
          }
        </g >
      </svg >
    );
  }
}

export default Pie;

Pie.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  innerRadius: PropTypes.number,
  labels: PropTypes.bool,
  totalColor: PropTypes.string,
  margin: PropTypes.object,
  expandHighlightRadius: PropTypes.number,
  yName: PropTypes.string,
  xName: PropTypes.string,
  onClick: PropTypes.func,
};

Pie.defaultProps = {
  colorRange: defaultColorRange,
  labels: true,
  innerRadius: 0.7,
  fontSize: null,
  totalColor: '#a0a0a0',
  expandHighlightRadius: 1.1,
  onClick: null,
  margin: {
    right: 20, left: 20,
    top: 20, bottom: 20
  },
  xName: 'name',
  yName: 'value'
};