import React  from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import LineChartNumber from "./LineChartNumber";


class LineChart extends LineChartNumber {

  constructor(props) {
    super(props);

    const {xName} = props;

    this.xScale = d3.scaleTime().rangeRound([this.appliedMargin.left, this.absWidth]);

    this.xAxis = d3.axisBottom(this.xScale)
      .tickFormat(d3.timeFormat('%d-%b'));

    this.formatDate = d3.timeFormat("%d-%b-%y");

    // Lines
    this.lineGenerator = d3
      .line()
      .x(d => this.xScale(d[xName]))
      .y(d => this.yScale(d.value));

  }

  componentDidUpdate() {
    if(this.refs.xAxis){
      d3.select(this.refs.xAxis).call(this.xAxis)
        .selectAll("text")
        .attr("dy", "1.7em")
        .attr("dx", "1.7em")
        .attr("transform", "rotate(45)")
      ;
    }
    if(this.refs.yAxis) {
      d3.select(this.refs.yAxis).call(this.yAxis);
    }

  }

  handleMouseMoveX = (event ) => {
    const  {x,offsetX} = this.getMousePosition(event);
    const {margin} = this.props;
    const {zoom} = this.state;
    const {isZooming, from} = zoom;

    if(isZooming && from){

      const width = Math.abs(from.offsetX-offsetX);

      if((offsetX) > from.offsetX ){
        zoom.overlay = {
          width,
          start: from.offsetX + margin.left,
          fromX: this.formatDate(from.x),
          toX: this.formatDate(x),
        };
      }else {
        zoom.overlay = {
          width,
          start: offsetX+ margin.left,
          fromX: this.formatDate(x),
          toX: this.formatDate(from.x),
        };
      }

    }
    else {
      zoom.overlay = null;
    }

    this.setState({ zoom})
  }



  readProps =(props) => {
    const lines = [];

    const {zoom} = this.state;

    const {data,width, height, colorRange,xName} = props;

    if(!data || !data.length) return {lines};
    const valueKeys = Object.keys(data[0]).filter(k=>k!==xName);
    if(!width) return {lines};
    if(!height) return {lines};
    let appliedData = data;
    if( zoom.to && !zoom.isZooming){
      appliedData = this.filterData(data, zoom, valueKeys, xName);
    }
    if(!appliedData || !appliedData.length) return {lines};

    const absMax = d3.max(appliedData, d => d3.max(valueKeys.map(k=>d[k])));
    const absMin = d3.min(appliedData, d => d3.min(valueKeys.map(k=>d[k])));

    // Update scales
    this.yScale.domain([absMin, absMax]);
    this.xScale.domain(d3.extent(appliedData, d => d[xName]));

    valueKeys.forEach((k,i)=> {

      const relData = appliedData.map((d) => ({[xName]: d[xName], value:d[k]}));
      relData.sort((a,b)=> a[xName]-b[xName]);

      const path = this.lineGenerator(relData);

      lines.push({
        name: k,
        path,
        color: colorRange[k] || colorRange[i] ,
        data: relData,
        transform: `translate(${0},${this.appliedMargin.top})`
      });

    })

    return {lines};
  }

  render() {
    const {width, height, id, margin,fontSize} = this.props;
    const {lines} = this.readProps(this.props);
    const {zoom} = this.state;

    return (
      <svg
        key={id}
        width={width}
        height={height}
        onClick={this.onClick}
        onMouseUp={this.handleMouseUp}
        onMouseDown={this.handleMouseDown}
        onMouseMove={this.handleMouseMoveX}>


        {lines.map((graph,i)=>(
          <g key={`group_${graph.name}`}>
            <path
              key={`p_${graph.name}`}
              transform={graph.transform}
              d={graph.path}
              fill="none"
              strokeLinejoin="round"
              strokeLinecap="round"
              strokeWidth={1.5}
              stroke={graph.color}/>
            <text
              key={`text_${graph.name}`}
              style={{fontSize: `${12}px`}}
              fill={graph.color}
              transform={`translate(${this.appliedMargin.left+10},${this.appliedMargin.top+ 30 + i*14})`}>
              <tspan x={0} y={0}>{graph.name}</tspan>
            </text>
          </g>
        ))}

        {zoom.isZooming && zoom.overlay ?
          <g>
            <rect
              transform={`translate(${zoom.overlay.start},${margin.top})`}
              width={zoom.overlay.width}
              height={height-margin.bottom-margin.top}
              opacity={0.3}
              fill="#909090" />
            <text
              style={{fontSize: `${fontSize}px`}}
              fill="#404040"
              transform={`translate(${zoom.overlay.start},${margin.top})`}>
              <tspan x={2} y={fontSize*2}>From {zoom.overlay.fromX}</tspan>
            </text>
            <text
              style={{fontSize: `${fontSize}px`}}
              fill="#404040"
              transform={`translate(${zoom.overlay.start},${margin.top})`}>
              <tspan x={2} y={fontSize*4}>To {zoom.overlay.toX}</tspan>
            </text>

          </g>
          : null}


        <g>
          <g ref="xAxis" transform={`translate(0,${this.yScale(0) + this.appliedMargin.top})`}/>
          <g ref="yAxis" transform={`translate(${this.appliedMargin.left},${margin.top})`} />
        </g>
      </svg>
    );
  }
}

export default LineChart;

LineChart.propTypes = {
  data: PropTypes.array.isRequired,
  id: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  xName: PropTypes.string.isRequired,
  margin: PropTypes.object.isRequired,
  colorRange: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  fontSize: PropTypes.number.isRequired,
  xType: PropTypes.string.isRequired,
};

LineChart.defaultProps = {
};