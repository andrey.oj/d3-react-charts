import React  from 'react';
import PropTypes from 'prop-types';
import LineChartDate from "./LineChartDate";
import LineChartNumber from "./LineChartNumber";
import {defaultMargin,defaultColorRange} from "./constants";
import {parserLineData} from "./parser";


const LineChart = (props) => {

  const {data,xName,xType, yNames} = parserLineData(props.data, props.stacked);

  return xType === 'date' ?
    <LineChartDate {...props} xName={xName} data={data} /> :
    <LineChartNumber {...props} xName={xName} data={data} xType={xType} yNames={yNames}/>;
}

export default LineChart;

LineChart.propTypes = {
  data: PropTypes.array.isRequired,
  id: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  margin: PropTypes.object,
  fontSize: PropTypes.number,
  colorRange: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  stacked: PropTypes.bool,
};

LineChart.defaultProps = {
  fontSize: 12,
  colorRange: defaultColorRange,
  margin : defaultMargin,
  type: 'line',
  stacked: false,
};