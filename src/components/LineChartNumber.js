import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import {defaultMargin,defaultColorRange} from "./constants";

class LineChart extends Component {

  constructor(props) {
    super();

    const {width, height, margin} = props;
    this.appliedMargin = margin || defaultMargin;

    this.absHeight = height-this.appliedMargin.top -this.appliedMargin.bottom;
    this.absWidth = width-this.appliedMargin.left -this.appliedMargin.right;

    this.xScale = d3.scaleLinear().rangeRound([this.appliedMargin.left, this.absWidth]);
    this.yScale = d3.scaleLinear().rangeRound([this.absHeight, this.appliedMargin.top]);

    this.xAxis = d3.axisBottom(this.xScale)
      .tickFormat( d=>`${d}`);
    this.yAxis = d3.axisLeft(this.yScale)
      .tickFormat(d=>`${d}`);

    this.formatDate = d3.timeFormat("%d-%b-%y");

    this.state = {
      pointer: null,
      zoom : {
        isZooming: false,
        from: null,
        to: null,
      }
    };
  }

  componentWillReceiveProps(props){
    const { xName,data, type} = props;

    if(data && data.length){
      if( type === 'line'){
        this.pathGenerator =
          d3
            .line()
            .x(d => this.xScale(d[xName]))
            .y(d => this.yScale(d.value));
      }
      else if( type === 'area'){
        this.pathGenerator =
          d3
            .area()
            .x(d => this.xScale(d[xName]))
            .y0(this.yScale(0))
            .y1(d => this.yScale(d.value));
      }
      this.data = data;
    }
  }

  componentDidUpdate() {
    if(this.refs.xAxis){
      d3.select(this.refs.xAxis).call(this.xAxis);
    }
    if(this.refs.yAxis) {
      d3.select(this.refs.yAxis).call(this.yAxis);
    }

  }


  getMousePosition = (event) => {
    const  {nativeEvent} = event;
    const {margin} = this.props;
    const {offsetX,offsetY} = nativeEvent;
    const x = this.xScale.invert(offsetX);
    const y = this.yScale.invert(offsetY);
    return {x,y, offsetX: offsetX - margin.left, offsetY};
  }

  handleMouseDown = (event) => {
    const  {x,y,offsetX} = this.getMousePosition(event);
    if( this.state.zoom.isZooming || this.state.zoom.to) {
      this.resetZoom();
      return;
    }
    const zoom = {
      isZooming: true,
      from: {x,y, offsetX},
      to: null,
    };
    this.setState({zoom})
  }

  handleMouseUp = (event) => {
    const  {x,y, offsetX} = this.getMousePosition(event);
    if(!this.state.zoom.from) return;

    const xMin = Math.min((x), (this.state.zoom.from.x));
    const xMax = Math.max((x), (this.state.zoom.from.x));
    const yMin = Math.min(y, this.state.zoom.from.y);
    const yMax = Math.max(y, this.state.zoom.from.y);

    const zoom = {
      isZooming: false,
      from: {x:xMin,y: yMin, offsetX},
      to: {y:yMax, x: xMax},
      overlay: null,
    };

    this.setState({zoom});
  }

  handleMouseMove = (event ) => {
    const  {x,offsetX} = this.getMousePosition(event);
    const {margin} = this.props;
    const {zoom} = this.state;
    const {isZooming, from} = zoom;

    if(isZooming && from){

      const width = Math.abs(from.offsetX-offsetX);

      if((offsetX) > from.offsetX ){
        zoom.overlay = {
          width,
          start: from.offsetX + margin.left,
          fromX: from.x.toFixed(1),
          toX: x.toFixed(1)
        };
      }else {
        zoom.overlay = {
          width,
          start: offsetX+ margin.left,
          fromX: x.toFixed(1),
          toX: from.x.toFixed(1)
        };
      }

    }
    else {
      zoom.overlay = null;
    }

    this.setState({ zoom})
  }

  resetZoom = () =>{
    this.setState({zoom: {isZooming: false,
      from: null,
      to: null,
      overlay: null,
    }});
  }

  onClick = () => {
    const {from,to} = this.state.zoom;
    if(!from || !to) {
      this.resetZoom();
      return;
    }

    const delta = Math.abs(from.x-to.x) + Math.abs(from.y-to.y);
    if( delta < 4){
      this.resetZoom();
    }
  }

  filterData = (data, zoom, valueKeys, xName) => {
    return data.filter(d=> {
      return ((d[xName]) > (zoom.from.x) && (d[xName]) <= (zoom.to.x));
    });
  }


  readProps =(props) => {
    const lines = [];
    const {zoom} = this.state;

    const {width, height, colorRange,xName,type, stacked,yNames} = props;
    const {data} = this;

    if(!data || !data.length || !yNames) return {lines};

    if(!width) return {lines};
    if(!height) return {lines};
    let appliedData = data;
    if( zoom.to && !zoom.isZooming){
      appliedData = this.filterData(data, zoom, yNames, xName);
    }

    if(!appliedData || !appliedData.length) return {lines, absMin: 0};

    const absMax = d3.max(appliedData, d => d3.max(yNames.map(k=>d[k])));
    const absMin = d3.min(appliedData, d => d3.min(yNames.map(k=>d[k])));

    //const stackedData = d3.stack().values(d=>appliedData);
    // Update scales
    this.yScale.domain([absMin, absMax]);
    this.xScale.domain(d3.extent(appliedData, d => d[xName]));

    yNames.forEach((k,i)=> {
      const relData =
        appliedData.map((d) => ({[xName]: d[xName], value:d[k]}));
      const color = colorRange[k] || colorRange[i] || defaultColorRange[i];

      const path = this.pathGenerator(relData);

      lines.push({
        name: k,
        path,
        color,
        data: relData,
        transform: `translate(${0},${this.appliedMargin.top})`
      });

    });

    return {lines, absMin, absMax};
  }

  render() {
    const {width, height, id, margin,type} = this.props;
    const {lines,absMin, absMax} = this.readProps(this.props);
    const {zoom} = this.state;

    let minLine = absMin > 0 ? absMin :0;
    if(absMax < 0) minLine = absMin;
    return (
      <svg
        key={id}
        width={width}
        height={height}
        onClick={this.onClick}
        onMouseUp={this.handleMouseUp}
        onMouseDown={this.handleMouseDown}
        onMouseMove={this.handleMouseMove}>

        {lines.map((graph,i)=>(
          <g key={`group_${graph.name}`}>
            <path
              key={`p_${graph.name}`}
              transform={graph.transform}
              d={graph.path}
              fill={type === 'area' ? graph.color : 'none'}
              strokeLinejoin="round"
              strokeLinecap="round"
              strokeWidth={1.5}
              stroke={graph.color}/>
            <text
              key={`text_${graph.name}`}
              style={{fontSize: `${12}px`}}
              fill={graph.color}
              transform={`translate(${this.appliedMargin.left+10},${this.appliedMargin.top+ 30 + i*14})`}>
              <tspan x={0} y={0}>{graph.name}</tspan>
            </text>
          </g>
        ))}

        {zoom.isZooming && zoom.overlay ?
          <g>
            <rect
              transform={`translate(${zoom.overlay.start},${margin.top})`}
              width={zoom.overlay.width}
              height={height-margin.bottom-margin.top}
              opacity={0.3}
              fill="#909090" />
            <text
              style={{fontSize: `${12}px`}}
              fill="#404040"
              transform={`translate(${zoom.overlay.start},${margin.top})`}>
              <tspan x={2} y={20}>{zoom.overlay.fromX} - {zoom.overlay.toX}</tspan>
            </text>

          </g>
          : null}


        <g>
          <g ref="xAxis" transform={`translate(0,${this.yScale(minLine) + this.appliedMargin.top})`}/>
          <g ref="yAxis" transform={`translate(${this.appliedMargin.left},${this.appliedMargin.top})`} />
        </g>
      </svg>
    );
  }
}

export default LineChart;

LineChart.propTypes = {
  data: PropTypes.array.isRequired,
  id: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  xName: PropTypes.string.isRequired,
  margin: PropTypes.object.isRequired,
  colorRange: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  fontSize: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  yNames: PropTypes.array.isRequired,
  stacked: PropTypes.bool.isRequired,
};

LineChart.defaultProps = {
};