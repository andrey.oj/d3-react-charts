import Pie from './components/Pie';
import LineChart from './components/LineChart';
import Treemap from './components/Treemap';
import BarChart from './components/BarChart';

exports.Pie= Pie;
exports.LineChart = LineChart;
exports.Treemap = Treemap;
exports.BarChart = BarChart;
