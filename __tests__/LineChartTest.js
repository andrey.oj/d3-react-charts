import {parserLineData} from '../src/components/parser';

const EXPECTED_OUT = [
  {day: 0, Sinus: 120, Cosinus: 20},
  {day: 1, Sinus: 140, Cosinus: 40}
];

const EXPECTED_STACKED = [
  {day: 0, Sinus: 120, Cosinus: 20},
  {day: 1, Sinus: 260, Cosinus: 60}
];

test('Do not changes if given correctly', () => {
  expect(parserLineData(EXPECTED_OUT, false).data).toEqual(EXPECTED_OUT)
});

test('yNames', () => {
  expect(parserLineData(EXPECTED_OUT, false).yNames).toEqual(['Sinus', 'Cosinus'])
});
test('xName', () => {
  expect(parserLineData(EXPECTED_OUT, false).xName).toEqual('day')
});

test('Stacked', () => {
  expect(parserLineData(EXPECTED_OUT, true).data).toEqual(EXPECTED_STACKED)
});

