import {parseBarChartData} from '../src/components/parser';

const testMultiBarDataResultSimple = [
  [120,43,67]
];

const testSingleBarDataFullArr = [
  {Windows: 120},
  {Windows: 43},
  {Windows: 67}
];

test('testSingleBarDataFullArr', () => {
  expect(parseBarChartData(testSingleBarDataFullArr).data).toEqual(testMultiBarDataResultSimple)
});


const testSingleBarDataSimple = [
  120,
  43,
  67
];

test('testSingleBarDataSimple', () => {
  expect(parseBarChartData(testSingleBarDataSimple).data).toEqual(testMultiBarDataResultSimple)
});



const testMultiBarDataResultMulti = [
  [120,43,67],
  [234,213,200]
];

const testMultiBarArrayStack = [
  {Windows: 120, Linux: 234},
  {Windows: 43, Linux: 213},
  {Windows: 67, Linux: 200}
];

test('testMultiBarArrayStack', () => {
  expect(parseBarChartData(testMultiBarArrayStack).data).toEqual(testMultiBarDataResultMulti)
});


const testMultiBarDataFullArr = {
  Windows: [
    120,
    43,
    67
  ],

  Linux: [
    234,
    213,
    200
  ],
};


test('testMultiBarDataFullArr', () => {
  expect(parseBarChartData(testMultiBarDataFullArr).data).toEqual(testMultiBarDataResultMulti)
});

