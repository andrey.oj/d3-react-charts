**d3-react-charts

# Installation

`
npm install --save d3-react-charts
`  
or  
`yarn add d3-react-charts`

Examples:  git@gitlab.com:andrey.oj/d3-react-charts-examples.git

# Usage

## Pie chart
![Pie chart](https://gitlab.com/andrey.oj/d3-react-charts/raw/master/images/pie.png)  

Reserved keys for input data: name, value  

**Required props:**  
    width: number  
    height: number  
    data: array   
    
**Default props:**  
fontSize: 12  
labels: true  
  innerRadius: 0.7  
  fontSize: null  
  totalColor: '#a0a0a0'  
  expandHighlightRadius: 1.1  
  colorRange: defaultColorRange  
  margin : defaultMargin  
  
```javascript
import {Pie} from 'd3-react-charts';

const testPieData = [
{name: 'Windows', value: 312},
{name: 'OSX', value: 43},
{name: 'Linux', value: 453},
{name: 'Android', value: 76},
{name: 'iOS', value: 12},
];  


class AnyClass extends React.Component {
  
  render(){
       return (
        <Pie
         data={testPieData}
         width={300}
         height={200}
         />
       );
   }
}

```


## Line chart
![Line chart](https://gitlab.com/andrey.oj/d3-react-charts/raw/master/images/line.png "Line chart")  
Reserved keys for input data: -  

**Required props:**  
    width: number  
    height: number  
    id: string  
    data: array   
    
**Default props:**  
fontSize: 12  
  xName:'date'  
  xType: 'date'  
  colorRange: defaultColorRange  
  margin : defaultMargin  
  
```javascript
import {Pie, LineChart} from 'd3-react-charts';

function randomLinedata() {
  const arr = [];
  for(let i =0;i< 100; i+=1){
    arr.push(i);
  }
  const date = new Date();
  return arr.map(i=>{
    const d = new Date();
    d.setTime(date.getTime() + i*86400000);
    return  ({
      date: d,
      Sinus: i*13.0 + Math.sin(i/4)*50-500,
      Randomized: i*Math.sqrt(i*2)+ Math.random()*40-500,
    });
  });
}


class AnyClass extends React.Component {
  
  render(){
       return (
        <LineChart
                 id="Line1"
                 data={randomLinedata()}
                 width={900}
                 height={400}
               />
       );
   }
}

```

## Treemap
![Treemap](https://gitlab.com/andrey.oj/d3-react-charts/raw/master/images/treemap.png "Tree map")  
Reserved keys for input data: -  

**Required props:**  
    width: number  
    height: number  
    data: array or object  
    
**Default props:**  
  labels: true  
  
```javascript
import {Treemap} from 'd3-react-charts';

//  Allowed data structures
const testTreemapData0 = [
{name: 'Windows', value: 312, category: 'PC'},
{name: 'OSX', value: 423, category: 'PC'},
{name: 'Linux', value: 234, category: 'PC'},
{name: 'Android', value: 76, category: 'Mobile'},
{name: 'iOS', value: 123, category: 'Mobile'},
];  

const testTreemapData1 = {
  Windows: 1023,
  Linux: 4535,
  OSX: 3144,
};

const testTreemapData2 = {
  Windows: {
    value: 424,
    category: 'PC'
  },

  Linux: {
    value: 3435,
    category: 'PC'
  },
  OSX: {
    value: 4329,
    category: 'PC'
  },
  Android: {
    value: 9874,
    category: 'Mobile'
  },

};

class AnyClass extends React.Component {
  
  render(){
       return (
        <Treemap
         data={testTreemapData0}
         width={800}
         height={300}
         />
       );
   }
}

```
## Bar chart
![Bar chart](https://gitlab.com/andrey.oj/d3-react-charts/raw/master/images/bar.png "Bar chart")  
Reserved keys for input data: name, value  

**Required props:**  
    width: number  
    height: number  
    data: array or object   
    
**Default props:**  
    colorRange: defaultColorRange,
    margin: defaultMargin,
    layout: 'stacked'   

  
```javascript
import {BarChart} from 'd3-react-charts';

const testBarData = {
  Windows: [
    120,
    43,
    67
  ],
  Linux: [
    234,
    213,
    200
  ],
};

const testBarData2 = [
    [120,43,67],
    [234,213,200]
];
const testBarData3 = [
  {Windows: 120, Linux: 234},
  {Windows: 43, Linux: 213},
  {Windows: 67, Linux: 200}
];

class AnyClass extends React.Component {
  
  render(){
       return (
        <BarChart
          data={testBarData}
          width={300}
          height={400}
        />
       );
   }
}

```
